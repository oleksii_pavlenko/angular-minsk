import * as z from 'zod';

const userAge = z.number().refine(val => val > 18, '18+');


const userSchema = z.object({
  firstName: z.string(),
  lastName: z.string(),
});

type User = z.infer<typeof userSchema>;
/*
equivalent to:
type Dog = {
  firstName:string;
  lastName: string;
}
*/
