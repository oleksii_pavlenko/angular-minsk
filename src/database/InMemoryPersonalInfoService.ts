import {InMemoryDbService} from 'angular-in-memory-web-api';

export class InMemoryPersonalInfoService implements InMemoryDbService {
  createDb() {
    const personalInfo = {
      isActiveUser: true,
      firstName: 'John',
      lastName: 32,
      middleName: 'JohnDoe',
      email: 'john.doe@mailinator.com',
      age: 30,
      pets: {
        name: 'Harry Potter',
        age: 3
      },
    };
    return {personalInfo};
  }
}
