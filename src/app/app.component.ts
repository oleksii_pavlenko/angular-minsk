import {Component} from '@angular/core';
import {AppService} from './app.service';
import {IPersonalInfo} from '../types/personal-info.class';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-minsk';
  public personalInfo: IPersonalInfo;
  public error: string;


  constructor(private readonly _appService: AppService) {
    this._appService.getPersonalInfo()
      .subscribe(
        (data) => {
          this.personalInfo = data;
        },
        (err => {
          if (err) {
            this.error = err;
          }
        }));
  }
}
