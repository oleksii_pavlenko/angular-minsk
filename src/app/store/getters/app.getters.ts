import {createFeatureSelector, createSelector} from '@ngrx/store';

import {IAppState} from '../reducers/app.reducers';

export const selectAppState = createFeatureSelector<IAppState>('app');

export const selectPersonalInfo = createSelector(
  selectAppState,
  (state) => state.personalInfo,
);

export const selectError = createSelector(
  selectAppState,
  (state) => state.error,
);
