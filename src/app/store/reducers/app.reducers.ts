import {Action, createReducer, on} from '@ngrx/store';
import * as appActions from '../actions/app.actions';
import {PersonalInfo} from '../../../types/personal-info.class';

export interface IAppState {
  personalInfo: PersonalInfo;
  error: string;
}

export const INITIAL_STATE: IAppState = {
  personalInfo: undefined,
  error: undefined,
};

export const appReducer = createReducer(
  INITIAL_STATE,
  on(appActions.setPersonalInfoAction, (state: IAppState, {personalInfo}) => ({...state, personalInfo})),
  on(appActions.setErrorAction, (state: IAppState, {error}) => ({...state, error})),
);

export function reducer(state: IAppState | undefined, action: Action): IAppState {
  return appReducer(state, action);
}
