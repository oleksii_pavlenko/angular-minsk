import {NgModule} from '@angular/core';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {AppEffects} from './effects/app.effects';
import * as app from './reducers/app.reducers';

@NgModule({
  imports: [
    StoreModule.forRoot({app: app.reducer}),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
    }),
    EffectsModule.forRoot([AppEffects])
  ],
  exports: [StoreModule],
})
export class AppStoreModule {
}
