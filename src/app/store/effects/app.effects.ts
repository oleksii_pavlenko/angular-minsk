import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {APP_ACTIONS, setErrorAction, setPersonalInfoAction} from '../actions/app.actions';
import {catchError, filter, map, mergeMap, withLatestFrom} from 'rxjs/operators';
import {selectPersonalInfo} from '../getters/app.getters';
import {AppService} from '../../app.service';
import {IAppState} from '../reducers/app.reducers';
import {Action, Store} from '@ngrx/store';
import {EMPTY, Observable, of} from 'rxjs';
import {PersonalInfo} from '../../../types/personal-info.class';


@Injectable()
export class AppEffects {

  loadCountries$ = createEffect(() => this._actions$.pipe(
    ofType(APP_ACTIONS.GET_PERSONAL_INFO),
    withLatestFrom(this._store.select(selectPersonalInfo)),
    filter(([, personalInfo]) => !personalInfo),
    mergeMap(() => this._appService.getPersonalInfo()
      .pipe(
        map((personalInfo) => setPersonalInfoAction({personalInfo})),
        catchError((error) => {
          return of(setErrorAction({error}));
        })
      )
    )
  ));

  constructor(
    private _actions$: Actions,
    private _appService: AppService,
    private _store: Store<IAppState>,
  ) {
  }
}
