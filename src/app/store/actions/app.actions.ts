import {createAction, props} from '@ngrx/store';
import {PersonalInfo} from '../../../types/personal-info.class';

export const APP_ACTIONS = {
  SET_PERSONAL_INFO: '[APP] Set Personal Info',
  GET_PERSONAL_INFO: '[APP] Get Personal Info',
  SET_ERROR: '[APP] SET ERROR',
  GET_ERROR: '[APP] Get ERROR',
};

export const setPersonalInfoAction = createAction(APP_ACTIONS.SET_PERSONAL_INFO, props<{personalInfo: PersonalInfo}>());
export const getPersonalInfoAction = createAction(APP_ACTIONS.GET_PERSONAL_INFO);
export const setErrorAction = createAction(APP_ACTIONS.SET_ERROR, props<{error: string}>());

