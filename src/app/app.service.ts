import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError, concatMap, flatMap, map} from 'rxjs/operators';
import {fromPromise} from 'rxjs/internal-compatibility';
import {EMPTY, Observable, of} from 'rxjs';
import {IPersonalInfo, PersonalInfo} from '../types/personal-info.class';
import {PersonalInfoDecoder} from '../decoders/personalInfo.decoders';

@Injectable()
export class AppService {
  constructor(public readonly _http: HttpClient) {
  }

  public getPersonalInfo(): Observable<IPersonalInfo> {
    return this._http.get<IPersonalInfo>('api/personalInfo')
      .pipe(
        concatMap(p => fromPromise(
          PersonalInfoDecoder
            .decodePromise(new PersonalInfo(p))
            .catch(e => {
              throw new Error(e);
          })
        ))
      );
  }
}
