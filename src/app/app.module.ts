import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryPersonalInfoService} from '../database/InMemoryPersonalInfoService';
import {AppStoreModule} from './store/app-store.module';
import {AppService} from './app.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppStoreModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryPersonalInfoService),
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
