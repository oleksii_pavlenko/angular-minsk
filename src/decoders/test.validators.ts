import Joi from '@hapi/joi';
import * as yup from 'yup';
import {string} from 'zod';
import * as t from 'io-ts';
import * as z from 'zod';
import {Record, String, Array, Static} from 'runtypes';

export const PetsSchema = z.object({
  name: z.string(),
  age: z.number(),
});

export const PersonalInfoSchema = z.object({
  isActiveUser: z.boolean(),
  firstName: z.string(),
  lastName: z.string(),
  middleName: z.string(),
  email: z.string(),
  age: z.number(),
  pets: PetsSchema,
});

export const PersonalInfoSchemaWithAddress = PersonalInfoSchema.augment({
  address: z.string(),
});

const agePromise = z.promise(z.number());

agePromise.parse('twenty');
// ZodError: Non-Promise type: string

agePromise.parse(Promise.resolve('twenty'));
// => Promise<number>

const test = async () => {
  await agePromise.parse(Promise.resolve('tuna'));
  // ZodError: Non-number type: string

  await agePromise.parse(Promise.resolve(20));
  // => 20
};



export const NoAddressSchema = PersonalInfoSchemaWithAddress.omit({ address: true });

const JustTheEmail = PersonalInfoSchema.pick({email: true});

type JustTheEmail = z.infer<typeof JustTheEmail>; // => { email: string }

const FavoriteUserColor = z.union([z.literal('red'), z.literal('black'), z.literal('white')]);

FavoriteUserColor.parse('white'); // => "white"
FavoriteUserColor.parse('brown'); // => throws


const age = z.number().refine(val => val >= 18, '18+');

const schema = Joi.object({
  username: Joi.string()
    .alphanum()
    .min(3)
    .max(30)
    .required(),

  password: Joi.string()
    .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),

  repeat_password: Joi.ref('password'),

  access_token: [
    Joi.string(),
    Joi.number()
  ],

  birth_year: Joi.number()
    .integer()
    .min(1900)
    .max(2013),

  email: Joi.string()
    .email({minDomainSegments: 2, tlds: {allow: ['com', 'net']}})
})
  .with('username', 'birth_year')
  .xor('password', 'access_token')
  .with('password', 'repeat_password');


const schemaUser = yup.object({
  name: yup.string(),
});
schemaUser.validate({}); // passes

type SchemaType = yup.InferType<typeof schemaUser>;
// returns { name: string }
// should be { name?: string }


const UserFirstName = t.type({
  firstName: t.string,
});
const UserLastName = t.partial({
  lastName: t.string,
});
const UserProfile = t.intersection([UserFirstName, UserLastName]);

const UserLocation = Record({
  address: String,
}).asReadonly();

const UserLocationList = Array(UserLocation);

const userLocationList: Static<typeof UserLocationList> = [];

userLocationList.push({address: 'Diagon Alley'});
