import {JsonDecoder} from 'ts.data.json';
import {IPets, PersonalInfo} from '../types/personal-info.class';

const PetsDecoder = JsonDecoder.object<IPets>(
  {
    name: JsonDecoder.string,
    age: JsonDecoder.number
  },
  'PetsDecoder'
);

export const PersonalInfoDecoder = JsonDecoder.object<PersonalInfo>({
  isActiveUser: JsonDecoder.boolean,
  firstName: JsonDecoder.string,
  lastName: JsonDecoder.string,
  middleName: JsonDecoder.optional(JsonDecoder.string),
  email: JsonDecoder.string,
  age: JsonDecoder.number,
  pets: PetsDecoder,
}, 'PersonalInfoDecoder');

