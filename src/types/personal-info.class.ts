import * as z from 'zod';
import {PersonalInfoSchema, PetsSchema} from '../decoders/test.validators';
import {JsonDecoder} from 'ts.data.json';

export interface IPets {
  name: string;
  age: number;
}

export interface IPersonalInfo {
  isActiveUser: boolean;
  firstName: string;
  lastName: string;
  middleName: string;
  email: string;
  age: number;
  pets: z.infer<typeof PetsSchema>;
}

export class PersonalInfo implements z.infer<typeof PersonalInfoSchema> {
  public readonly isActiveUser: boolean;
  public readonly firstName: string;
  public readonly lastName: string;
  public readonly middleName: string;
  public readonly email: string;
  public readonly age: number;
  public readonly pets: z.infer<typeof PetsSchema>;

  constructor(data: IPersonalInfo) {
    this.isActiveUser = data.isActiveUser;
    this.firstName = data.firstName;
    this.lastName = data.lastName;
    this.middleName = data.middleName;
    this.email = data.email;
    this.age = data.age;
    this.pets = data.pets;
  }
}
